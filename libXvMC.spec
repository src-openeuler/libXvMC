Name:          libXvMC
Version:       1.0.14
Release:       1
Summary:       X-Video Motion Compensation API
License:       MIT
URL:           https://www.x.org
Source0:       https://xorg.freedesktop.org/archive/individual/lib/%{name}-%{version}.tar.gz

BuildRequires: autoconf automake libtool xorg-x11-util-macros
BuildRequires: pkgconfig(videoproto) pkgconfig(xv) libX11-devel
Requires:      libX11

%description
XvMC is an extension of the X video extension (Xv) for the X Window System.
The XvMC API allows video programs to offload portions of the video decoding
process to the GPU video-hardware.

%package       devel
Summary:       Development package for libXvMC
Requires:      %{name} = %{version}-%{release}

%description devel
The development package for libXvMC.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -vif
%configure
%make_build

%check
make check

%install
%make_install
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/X11
touch $RPM_BUILD_ROOT%{_sysconfdir}/X11/XvMCConfig

%delete_la

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%license COPYING
%{_libdir}/*.so.*
%ghost %config(missingok,noreplace) %verify (not md5 size mtime) %{_sysconfdir}/X11/XvMCConfig

%files devel
%defattr(-,root,root)
%{_libdir}/{*.so,*.a}
%{_libdir}/pkgconfig/*.pc
%{_includedir}/X11/extensions/*.h
%exclude %{_docdir}/*/*.txt

%files help
%defattr(-,root,root)
%doc README.md XvMC_API.txt

%changelog
* Thu Mar 14 2024 liweigang <liweiganga@uniontech.com> - 1.0.14-1
- update to version 1.0.14

* Thu Mar 16 2023 zhangpan <zhangpan103@h-partners.com> - 1.0.13-2
- enable test

* Sun Jul 31 2022 tianlijing <tianlijing@kylinos.cn> - 1.0.13-1
- update to 1.0.13

* Fri Jul 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0.12-1
- updata package

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.11-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Mon Sep 09 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.11-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: rebuilt spec, add help package.


* Mon Aug 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.10-8
- Package init

